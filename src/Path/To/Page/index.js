import { Elm } from './Index.elm'
import { Elm as Menu } from '/Menu.elm'

Elm.Path.To.Page.Index.init({
  node: document.querySelector('main'),
})

Menu.Menu.init({
  node: document.querySelector('menu'),
})
