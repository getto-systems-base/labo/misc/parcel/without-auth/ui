import { Elm } from './Index.elm'
import { Elm as Menu } from '/Menu.elm'

Elm.Index.init({
  node: document.querySelector('main'),
})

Menu.Menu.init({
  node: document.querySelector('menu'),
})
